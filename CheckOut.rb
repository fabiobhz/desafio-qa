class CheckOut

  @memory
  @prices
  @rules

  def initialize(prices, rules)
    @memory = Hash.new()
    @prices = prices
    @rules = rules
  end

  def scan(item)
    #Verifica se se o item já foi adicionado uma vez a compra
    if (@memory.has_key?(item))
      #Se foi adicionado adiciona mais um na quantidade
      @memory[item] += 1
    else
      #Se é a primeira vez, adiciona com a quantidade 1
      @memory.store(item, 1)
    end
  end

  def total
    total = 0
    @memory.each { |key, value|
      #Checa de existe regra especial de valor
      if @rules.has_key?(key)
        #Se existir carrega a regra da lista de regras
        rule = @rules[key]
        #Quebra pelo sinal de igual, já que a regra está no modelo X=Y
        rule_definitions = rule.split('=')
        #Descobre quantos combos teem na compra, dividindo pelo agrupamento
        combos_count = (value / rule_definitions[0].to_i).floor
        #Descobre quantos itens avulsos existem além dos combos, usando resto (mod)
        remaing_count = (value % rule_definitions[0].to_i)
        #Multiplica combos pelo valor do combo e avulsos pelo valor avulsos e soma
        total += (@prices[key] * remaing_count) + (rule_definitions[1].to_i * combos_count)
      else
        #Se não houver regra, apenas multiplica e soma.
        total += (@prices[key] * value)
      end
    }
    return total
  end

end