# encoding: UTF-8
# language: pt

Como um usuário do aplicativo Whatsapp iOS
Quero criar um novo grupo
Para manter contato com meus amigos

Funcionalidade: Novo grupo
	O usuário poderá acessar a tela de conversas, onde poderá criar um novo grupo por meio de um botão apresentado no canto superior direito.
	
	Contexto: 
	Dado que abri o aplicativo Whatsapp
	E toco no item "conversas" na tabBar
	
	Cenário: Quando eu tocar no link "Novo Grupo"
	E selecionar o contato "Leonardo"
	E selecionar o contato "Maria"
	E selecionar o contato "João"
	E tocar no botão "seguinte"
	E preencher o campo de nome do grupo com "Família"
	E tocar no botão "criar"
	Então eu visualizo a mensagem "Você criou o grupo "Família""
	