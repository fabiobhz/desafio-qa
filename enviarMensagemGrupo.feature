# encoding: UTF-8
# language: pt

Como um usuário do aplicativo Whatsapp iOS
Quero enviar uma mensagem para o meu novo grupo
Para manter contato com meus amigos

Funcionalidade: Enviar mensagem
	O usuário poderá acessar a tela de conversas, onde poderá visualizar o grupo criado e, tocando no grupo, enviar e receber mensagens entre os contatos adicionados no grupo
	
	Contexto: 
	Dado que abri o aplicativo Whatsapp
	E toco no item "conversas" na tabBar
	
	Cenário: Quando eu preencher o campo de busca com "Família"
	E selecionar a conversa "Família"
	E preencher o campo de texto com "Olá família!"
	E tocar no botão enviar
	Então devo visualizar a mensagem "Olá família" na tela da conversa
	